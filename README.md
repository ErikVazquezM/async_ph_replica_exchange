This Python code allows you run asynchoronous replica exchange simulations (details are given in the reference below).

If you publish work using the code, please cite the following article:

Jack A. Henderson, Neha Verma, Robert Harris, Ruibin Liu, and Jana Shen,
"Assessment of Proton-Coupled Conformational Dynamics of SARS and MERS Coronavirus Papain-like Proteases: Implication for Designing Broad-Spectrum Antiviral Inhibitors"
J. Chem. Phys. In press. 2020. 
doi: https://doi.org/10.1101/2020.06.30.181305 

To run the example replica exchange simulations:

python run_replica_exchange.py


